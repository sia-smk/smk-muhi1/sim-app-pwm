@extends('layouts.master')

@section('title', 'register')

@section('theme-color', '#ffffff')

@section('css-page')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/register.css') }}" />
@endsection

@section('main')
    <div class="register-page">
        <div class="register-header">
            <div class='row'>
                <div class="col-3">
                    <a href="" class="">
                    <img src="{{asset('icon/logo-sekolah/logo-muhi.png')}}"/>
                    </a>
                </div>
      
                <div class="col-9 col-md-7">
                    <h6 class="register-title text-left text-danger">
                    SMK MUHAMMADIYAH 1 WELERI
                    </h6>
                </div>
            </div>
        </div>
        <div class="register-body">
            <h4 class="register-title">REGISTER</h4>
            <p class="register-description">
                Silahkan register ke akun Anda
            </p>
            <form action="javascript:void(0);" method="POST" id="form-register">
                @csrf
                <div class="form-group mt-5">
                    <label for="register-phone" class="label-register">No Telepon</label>
                    <input type="tel" class="form-control input-register" name="phone" id="register-phone" placeholder="No Telepon" oninput="this.value = onlyNumber(this.value)"/>
                    <img src="{{ asset('assets/images/icon_clear_input_text.svg') }}" id="clear-input-register-phone" class="clear-input-register-phone" style="display:none" alt="">
                    <div class="error-text" id="error-text-register-phone" style="display:none"></div>
                </div>
                <button class="btn btn-danger btn-block btn-rounded btn-register mt-4">Lanjut</button>
            </form>
        </div>
        <div class="register-footer">
            {{-- <div class="text-center">
                <p class="title-footer-other-register">Atau daftar Dengan</p>
                <img src="@asset('assets/images/button_rounded_facebook.svg')" alt="">
                <img src="@asset('assets/images/button_rounded_google.svg')" alt="">
                <img src="@asset('assets/images/button_rounded_twitter.svg')" alt="">
            </div> --}}
            <p class="answer-register">Belum punya Akun? <a href="">Daftar</a></p>
        </div>
    </div>
@endsection

@section('js-inline')
    <script>
        $(function () {
            $('#form-register').on('submit', function (e) {
                e.preventDefault();

                if($('#register-phone').val().length == 0) {
                    setError('Data wajib diisi.');
                } else {
                    if($('#register-phone').val().length < 8) {
                        setError('Minimal 8 digit.');
                    } else if($('#register-phone').val().length > 18) {
                        setError('Maksimal 18 digit.');
                    } else {
                        if($('#register-phone').val().match('^08') || $('#register-phone').val().match('^62') || $('#register-phone').val().match('^\\+62')) {
                            register();
                        } else {
                            setError('Format No Telpon tidak 08/62/+62');
                        }
                    }
                }
            });
        });

        function register() {
            var formData = $('#form-register').serialize();

            $.ajax({
                url: "@route('register')",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("input").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                    $('.btn-register').addClass('btn-progress');
                },
                complete() {
                    $("input").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                    $('.btn-register').removeClass('btn-progress');
                },
                success(result) {
                    if(result['success']){
                        if(result.message == 'Phone not register!') {
                            setError('Nomor HP Belum Terdaftar');
                        } else {
                            window.location="";
                        }
                    } else {
                        if(result.message = 'Phone is already registered') {
                            window.location="";
                        } else {
                            notification(result['message']);
                        }
                    }
                },
                error(xhr, status, error) {
                    var err = eval('(' + xhr.responseText + ')');
                    notification(err.message);
                    checkCSRFToken(err.message);
                }
            });
        }

        function setError(errorText) {
            $('#form-register .form-group').addClass('input-error');
            $('#error-text-register-phone').show();
            $('#error-text-register-phone').text(errorText);
        }

        $('#register-phone').on('keyup', function (e) {
            if(e.key !== 'Enter'){
                if(this.value.length >= 1) {
                    $('#clear-input-register-phone').fadeIn('fast');
                    $('#form-register .form-group').removeClass('input-error');
                    $('#error-text-register-phone').hide();
                } else if(this.value.length === 0 ) {
                    $('#clear-input-register-phone').fadeOut('fast');
                }
            }
        });

        $('#clear-input-register-phone').on('click', function () {
            $('#register-phone').val('');
            $('#clear-input-register-phone').fadeOut('fast');
            $('#form-register .form-group').removeClass('input-error');
            $('#error-text-register-phone').hide();
        });


        function onlyNumber(value) {
            value = value.replace(/[^\+0-9.]/g, ''); value = value.replace(/(\..*)\./g, '$1');

            return value;
        }
    </script>
@endsection
