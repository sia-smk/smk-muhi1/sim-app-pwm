@extends('layouts.master')

@section('title', 'Login')

@section('theme-color', '#ffffff')

@section('css-page')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/login.css') }}" />
@endsection

@section('main')
    <div class="login-page">
        <div class="login-header">
            <div class='row'>
                <div class="col-3">
                    <a href="" class="">
                    <img src="{{asset('icon/logo-sekolah/logo-muhi.png')}}"/>
                    </a>
                </div>
      
                <div class="col-9 col-md-7">
                    <h6 class="text-left text-danger">
                    SMK MUHAMMADIYAH 1 WELERI
                    </h6>
                </div>
            </div>
        </div>
        <div class="login-body">
            <h4 class="login-title">LOGIN</h4>
            <p class="login-description">
                Silahkan Login ke akun Anda
            </p>
            <form action="javascript:void(0);" method="POST" id="form-login">
                @csrf
                <div class="form-group mt-5">
                    <label for="login-phone" class="label-login">No Telepon</label>
                    <input type="tel" class="form-control input-login" name="phone" id="login-phone" placeholder="No Telepon" oninput="this.value = onlyNumber(this.value)"/>
                    <img src="{{ asset('assets/images/icon_clear_input_text.svg') }}" id="clear-input-login-phone" class="clear-input-login-phone" style="display:none" alt="">
                    <div class="error-text" id="error-text-login-phone" style="display:none"></div>
                </div>
                <button class="btn btn-danger btn-block btn-rounded btn-login mt-4">Lanjut</button>
            </form>
        </div>
        <div class="login-footer">
            {{-- <div class="text-center">
                <p class="title-footer-other-login">Atau daftar Dengan</p>
                <img src="@asset('assets/images/button_rounded_facebook.svg')" alt="">
                <img src="@asset('assets/images/button_rounded_google.svg')" alt="">
                <img src="@asset('assets/images/button_rounded_twitter.svg')" alt="">
            </div> --}}
            <p class="answer-register">Belum punya Akun? <a href="">Daftar</a></p>
        </div>
    </div>
@endsection

@section('js-inline')
    <script>
        $(function () {
            $('#form-login').on('submit', function (e) {
                e.preventDefault();

                if($('#login-phone').val().length == 0) {
                    setError('Data wajib diisi.');
                } else {
                    if($('#login-phone').val().length < 8) {
                        setError('Minimal 8 digit.');
                    } else if($('#login-phone').val().length > 18) {
                        setError('Maksimal 18 digit.');
                    } else {
                        if($('#login-phone').val().match('^08') || $('#login-phone').val().match('^62') || $('#login-phone').val().match('^\\+62')) {
                            login();
                        } else {
                            setError('Format No Telpon tidak 08/62/+62');
                        }
                    }
                }
            });
        });

        function login() {
            var formData = $('#form-login').serialize();

            $.ajax({
                url: "@route('login')",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("input").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                    $('.btn-login').addClass('btn-progress');
                },
                complete() {
                    $("input").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                    $('.btn-login').removeClass('btn-progress');
                },
                success(result) {
                    if(result['success']){
                        if(result.message == 'Phone not register!') {
                            setError('Nomor HP Belum Terdaftar');
                        } else {
                            window.location="";
                        }
                    } else {
                        if(result.message = 'Phone is already registered') {
                            window.location="";
                        } else {
                            notification(result['message']);
                        }
                    }
                },
                error(xhr, status, error) {
                    var err = eval('(' + xhr.responseText + ')');
                    notification(err.message);
                    checkCSRFToken(err.message);
                }
            });
        }

        function setError(errorText) {
            $('#form-login .form-group').addClass('input-error');
            $('#error-text-login-phone').show();
            $('#error-text-login-phone').text(errorText);
        }

        $('#login-phone').on('keyup', function (e) {
            if(e.key !== 'Enter'){
                if(this.value.length >= 1) {
                    $('#clear-input-login-phone').fadeIn('fast');
                    $('#form-login .form-group').removeClass('input-error');
                    $('#error-text-login-phone').hide();
                } else if(this.value.length === 0 ) {
                    $('#clear-input-login-phone').fadeOut('fast');
                }
            }
        });

        $('#clear-input-login-phone').on('click', function () {
            $('#login-phone').val('');
            $('#clear-input-login-phone').fadeOut('fast');
            $('#form-login .form-group').removeClass('input-error');
            $('#error-text-login-phone').hide();
        });


        function onlyNumber(value) {
            value = value.replace(/[^\+0-9.]/g, ''); value = value.replace(/(\..*)\./g, '$1');

            return value;
        }
    </script>
@endsection
