@extends('layouts.master_navbar')

@section('title', 'Verifikasi')

@section('css-page')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/otp.css') }}" />
@endsection

@section('navbar')
    <nav class="navbar fixed-top navbar-primary bg-primary">
        <div class="container">
            <a href="" class="nav-link-back">
                <img src="{{ asset('assets/images/icon_back.svg') }}" alt="" class="icon-back"/>
            </a>
            <h6 class="navbar-title">Login</h6>
            <div style="min-width:24px;"></div>
        </div>
    </nav>
@endsection

@section('main')
    <div class="otp-page">
        <div class="otp-header">
            <h4 class="otp-title">VERIFIKASI</h4>
            <p class="otp-description">
                Masukkan kode OTP yang telah dikirimkan ke nomor berikut
                ini:
            </p>
        </div>
        <form action="javascript:void(0);" id="form-verification" method="POST">
            @csrf
            <div class="otp-body">
                <h5 class="otp-number-phone">@{{ $phone }}</h5>
                <div class="otp-input-group">
                    <input type="tel" class="otp-input-code" name="otp1" id="input-otp-1" maxlength="1" onkeyup="insertOTP(this)" oninput="this.value = onlyNumber(this.value)" onfocus="checkValueOTP(this)" onfocusout="checkValueOTP(this, 'out')"/>
                    <input type="tel" class="otp-input-code" name="otp2" id="input-otp-2" maxlength="1" onkeyup="insertOTP(this)" oninput="this.value = onlyNumber(this.value)" onfocus="checkValueOTP(this)" onfocusout="checkValueOTP(this, 'out')"/>
                    <input type="tel" class="otp-input-code" name="otp3" id="input-otp-3" maxlength="1" onkeyup="insertOTP(this)" oninput="this.value = onlyNumber(this.value)" onfocus="checkValueOTP(this)" onfocusout="checkValueOTP(this, 'out')"/>
                    <input type="tel" class="otp-input-code" name="otp4" id="input-otp-4" maxlength="1" onkeyup="insertOTP(this)" oninput="this.value = onlyNumber(this.value)" onfocus="checkValueOTP(this)" onfocusout="checkValueOTP(this, 'out')"/>
                </div>
                <div class="error-text" id="error-text-verification-otp" style="display:none;">Kode OTP tidak valid.</div>
                <p class="otp-notification-countdown" id="countdown-page" style="display:none">
                    Mohon tunggu <span class="otp-countdown"></span> detik untuk mengirim ulang
                </p>
                <div class="resend-page" id="resend-page" style="display:none">
                    <p class="otp-answer-otp">Tidak menerima kode OTP?</p>
                    <a href="#" class="otp-resend-text" id="btn-resend-otp">Kirim Ulang</a>
                </div>
            </div>
            <div class="otp-footer">
                <button class="btn btn-primary btn-block btn-rounded" id="btn-verification-otp">
                    Konfirmasi
                </button>
            </div>
        </form>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalSendWhatsappOTP" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-sm modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <img src="{{ asset('/assets/images/icon_whatsapp.svg') }}" alt="">
                    <h6 class="title-modal-otp">Kode OTP akan dikirimkan via Whatsapp</h6>
                    <div class="footer-modal-otp">
                        <a href="" class="btn btn-outline-primary btn-rounded btn-block btn-other-otp">Cara Lain</a>
                        <button class="btn btn-rounded btn-primary btn-block btn-continue-otp" id="btn-request-otp">Lanjut</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-inline')
    <script>
        var request_again = "{{ $request_again }}";

        $(function () {
            $('#modalSendWhatsappOTP').modal('show');
            if(request_again != 0) {
                setTimer(request_again, '.otp-countdown');
            }

            $('#btn-request-otp').on('click', function () {
                $('#modalSendWhatsappOTP').modal('hide');

                requestOTP();
            });

            $('#btn-resend-otp').on('click', function () {
                $('#form-verification')[0].reset();
                $("#form-verification input[type=tel]").css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');

                requestOTP();
            });

            $('#form-verification').on('submit', function (e) {
                e.preventDefault();

                if($('#input-otp-1').val().length == 0 || $('#input-otp-2').val().length == 0 || $('#input-otp-3').val().length == 0 || $('#input-otp-4').val().length == 0) {
                    setError('Data wajib diisi.');
                } else {
                    verificationOtp();
                }
            });
        });

        function requestOTP() {
            $.ajax({
                url: "{{ route('login.verification.request') }}",
                type: "POST",
                dataType: "json",
                data: {
                    'method': 'whatsapp',
                    '_token': '{{ csrf_token() }}'
                },
                beforeSend() {
                    // $('.loading-page').show();
                    $("input").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                },
                complete() {
                    $("input").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                    // $('.loading-page').fadeOut();
                },
                success(result) {
                    if(result['success']){
                        setTimer(result['data']['request_again'], '.otp-countdown');
                        setTimeout(() => {
                            $('#input-otp-1').focus();
                        }, 50);
                    } else {
                        if(result['message'].toLowerCase() == 'phone not register!') {
                            notification('Nomor telepon belum terdaftar!');
                            window.location="@route('login')?login=new";
                        } else {
                            notification(result['message']);
                        }
                    }
                },
                error(xhr, status, error) {
                    var err = eval('(' + xhr.responseText + ')');
                    notification(err.message);
                    checkCSRFToken(err.message);
                }
            });
        }

        function verificationOtp() {
            var formData = $('#form-verification').serialize();

            $.ajax({
                url: "@route('login.verification')",
                type: "POST",
                dataType: "json",
                data: formData,
                beforeSend() {
                    $("input").attr('disabled', 'disabled');
                    $("button").attr('disabled', 'disabled');
                    $('#btn-verification-otp').addClass('btn-progress');
                },
                complete() {
                    $("input").removeAttr('disabled', 'disabled');
                    $("button").removeAttr('disabled', 'disabled');
                    $('#btn-verification-otp').removeClass('btn-progress');
                },
                success(result) {
                    if(result['success']){
                        window.location="";
                    } else {
                        setTimeout(() => {
                            $('#input-otp-1').val('');
                            $('#input-otp-2').val('');
                            $('#input-otp-3').val('');
                            $('#input-otp-4').val('');
                            $('#input-otp-1').css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');
                            $('#input-otp-2').css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');
                            $('#input-otp-3').css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');
                            $('#input-otp-4').css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');
                        }, 50);
                        if(result['message'].toLowerCase() == 'otp expired') {
                            setError('Kode OTP Kadaluarsa');
                        }
                        if(result['message'].toLowerCase() == 'otp not match') {
                            setError('Kode OTP tidak valid');
                        }
                    }
                },
                error(xhr, status, error) {
                    var err = eval('(' + xhr.responseText + ')');
                    notification(err.message);
                    checkCSRFToken(err.message);
                }
            });
        }

        function setTimer(duration, display) {
            var timer = duration;

            $('#countdown-page').show();
            $('#resend-page').hide();

            intervalCountdown = setInterval(() => {
                $(display).text(timer);

                if(--timer < 0) {
                    $('#countdown-page').hide();
                    $('#resend-page').show();
                    clearInterval(intervalCountdown);
                }
            }, 1000);
        }

        function setError(errorText) {
            $('.otp-body .otp-input-group').addClass('input-error');
            $('#error-text-verification-otp').show();
            $('#error-text-verification-otp').text(errorText);
        }

        function insertOTP(object) {
            $(document).keyup(function (e) {
                if(e.keyCode != 13) {
                    $('.otp-body .otp-input-group').removeClass('input-error');
                    $('#error-text-verification-otp').hide();
                }
            });

            if (object.value.length > 1) {
                object.value = object.value[object.value.length - 1];
            }
            try {
                if (object.value == null || object.value == "") {
                    if (object.previousElementSibling != null) {
                        focusOnInput(object.previousElementSibling);
                    }
                } else {
                    if (object.nextElementSibling != null) {
                        focusOnInput(object.nextElementSibling);
                    }
                }
            } catch (e) {
                console.log(e);
            }

            var value = object.value;

            if(value == null || value == '') {
                $(object).css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');
            } else {
                $(object).css('border-bottom', '2px solid #3bb6dd');
            }
        }

        function focusOnInput(element) {
            element.focus();
            let val = element.value;
            element.value = "";
            setTimeout(() => {
                element.value = val;
            });
        }

        function checkValueOTP(object, focus = 'in') {
            var value = object.value;

            if(focus == 'in') {
                $(object).css('border-bottom', '2px solid #3bb6dd');
            } else {
                if(value == null || value == '') {
                    $(object).css('border-bottom', '2px solid rgba(0, 0, 0, 0.1)');
                } else {
                    $(object).css('border-bottom', '2px solid #3bb6dd');
                }
            }
        }

        function onlyNumber(value) {
            value = value.replace(/[^0-9.]/g, ''); value = value.replace(/(\..*)\./g, '$1');

            return value;
        }
    </script>
@endsection

