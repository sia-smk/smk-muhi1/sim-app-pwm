<div class="loading-page" style="display:none">
    <div class="loading-dialog">
        <div class="loading-content">
            <div class="lds-spinner">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
</div>
