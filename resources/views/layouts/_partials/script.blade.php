<!-- General JS Scripts -->
<script src="{{ asset('assets/js/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/bootstrap.min.js') }}"></script>
<!-- <script src="{{ asset('assets/js/service-worker.js') }}"></script> -->

<!-- JS Libraries -->
<!-- <script src="asset('assets/lib/js-cookie-2.2.1/js/js.cookie.js')"></script> -->
<script src="{{ asset('assets/lib/momentjs/js/moment-with-locales.js') }}"></script>
@yield('js-library')

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<!-- <script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-app.js"></script> -->
<!-- <script src="https://www.gstatic.com/firebasejs/6.2.0/firebase-messaging.js"></script> -->

<!-- Template JS -->

<!-- JS Pages -->
@yield('js-page')

<script>

    function checkCSRFToken(errorMessage = "")
    {
        if(errorMessage == 'CSRF token mismatch.') {
            location.reload();
        }
    }

    function numberWithDots(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

</script>

@yield('js-inline')
