<!-- General CSS -->
<link rel="stylesheet" href="{{ asset('assets/dist/css/bootstrap.min.css') }}" />

<!-- CSS Libraries -->
@yield('css-library')

<!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" />

<!-- CSS Pages -->
@yield('css-page')

<!-- CSS Inline -->
@yield('css-inline')
