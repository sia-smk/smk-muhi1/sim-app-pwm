<tr>
    <td style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative;">
        <table class="footer" align="center" width="294" cellpadding="0" cellspacing="0" role="presentation" style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 294px; margin: 0 auto; padding: 0; text-align: center; width: 294px;">
            <tr>
                <td class="content-cell" align="center" style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; max-width: 100vw; padding: 17px 18px;">
                    <p style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; line-height: 11px; margin-top: 0; color: rgba(0, 0, 0, 0.3); font-size: 8px; text-align: center;">
                        Alun-alun Utara Bumipala Vida Bekasi Mustika Jaya - Padurenan Bekasi Timur 17156 Jawa Barat Indonesia
                    </p>
                    <a href="https://instagram.com/waste4change" style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; text-decoration: none; display: inline-block;margin:8px 2px;">
                        <img src="https://pwm.waste4change.tech/icon/social/instagram.png" alt="" style="position:relative;min-width:32.7px;min-height:32.7px">
                    </a>
                    <a href="https://www.facebook.com/waste4change" style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; text-decoration: none; display: inline-block;margin:8px 2px;">
                        <img src="https://pwm.waste4change.tech/icon/social/facebook.png" alt="" style="position:relative;min-width:32.7px;min-height:32.7px">
                    </a>
                    <a href="https://www.linkedin.com/company/waste4change/" style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; text-decoration: none; display: inline-block;margin:8px 2px;">
                        <img src="https://pwm.waste4change.tech/icon/social/linkedin.png" alt="" style="position:relative;min-width:32.7px;min-height:32.7px">
                    </a>
                    <p style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; line-height: 17px; margin-top: 10px; color: rgba(0, 0, 0, 0.5); font-size: 10px; text-align: center;margin-bottom:5px;">
                        Jika menemukan masalah, Anda bisa menghubungi
                    </p>
                    <a href="https://wa.me/6285640372959" style="box-sizing: border-box; font-family: 'Nunito', sans-serif; position: relative; text-decoration: none; display: inline-block;margin:0;color:#30AEE4;font-size: 10px;text-align: center;line-height: 17px;">
                        Pusat Bantuan
                    </a>
                </td>
            </tr>
        </table>
    </td>
</tr>
