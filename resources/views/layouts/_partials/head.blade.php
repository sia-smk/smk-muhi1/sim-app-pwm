<meta charset="UTF-8" />
{{-- <meta name="viewport" content="width=device-width, initial-scale=1.0" /> --}}
<title>@yield('title')</title>
{{-- <link rel="apple-touch-icon" sizes="180x180" href="@asset('assets/images/favicon/apple-touch-icon.png')"> --}}
{{-- <link rel="icon" type="image/png" sizes="32x32" href="@asset('assets/images/favicon/favicon-32x32.png')"> --}}
{{-- <link rel="icon" type="image/png" sizes="16x16" href="@asset('assets/images/favicon/favicon-16x16.png')"> --}}
{{-- <link rel="manifest" href="@asset('manifest.webmanifest')"> --}}

<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="msapplication-starturl" content="/">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="theme-color" content="@yield('theme-color', '#30aee4')">
<link href="{{ asset('assets/lib/fontawesome/css/solid.css') }}" rel="stylesheet">
