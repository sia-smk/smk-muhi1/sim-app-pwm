<div class="notification-page" id="notification-popup" style="display:none;">
    <div class="notification-dialog">
        <div class="notification-content">
            <div class="notification-text">Berhasil salin no rekening bank</div>
            <svg onclick="dismissNotification();" width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M13.5 4.5L4.5 13.5" stroke="#B5B5B5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                <path d="M4.5 4.5L13.5 13.5" stroke="#B5B5B5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </div>
    </div>
</div>
