<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        @include('layouts._partials.head')
        @include('layouts._partials.style')
    </head>
    <body>
        <div id="app" class="app app-navbar container @yield('class-app')">
            @yield('navbar')

            <main class="main-app">
                @yield('main')
            </main>

            @include('layouts._partials.loading_page')

            @include('layouts._partials.notification')
        </div>

        @include('layouts._partials.script')
    </body>
</html>
