@extends('layouts.master_bottom_navigation')

@section('title', 'Home')

@section('theme-color', '#ffff')

@section('css-library')
    <link rel="stylesheet" href="{{ asset('assets/lib/swiper-5.4.5/css/swiper.min.css') }}" />
@endsection

@section('css-page')
    <link rel="stylesheet" href="{{ asset('assets/css/pages/home.css') }}" />
@endsection

@section('class-app','no-padding')

@section('main')
    <div class="home-page">
        <div class="header">
            <div class="col-12 banner-home">
                <h6 class="text-bold text-white mt-5">
                    Hi, Kenya Zahra
                </h6>
                <p class="text-white" style="font-weight: lighter">
                    Selamat datang di SMK Muhammadiyah 1 Weleri. Selamat belajar.....
                </p>
            </div>
        </div>


        <div class="account-statistic">
            <div class="nothing-subscribe" id="page-nothing-contract" style="display:none;">
                <div class="content">
                    <img src="{{ asset('assets/images/image_nothing_subscribe.svg') }}" alt="">
                    <div class="text-content">
                        <div class="title">Oops !</div>
                        <div class="description">Saat ini Anda tidak memiliki layanan aktif</div>
                    </div>
                </div>
                <a href="" class="btn btn-primary btn-rounded btn-block btn-subscribe">Berlangganan Sekarang</a>
            </div>
            
            <div class="">
                <p class="judul-section">Fitur buat kamu</p>
                <p class="text-muted">Fitur menarik mananti kamu nih</p>
            </div>
            <div class="new-service-request">
                <div class="text-center" style="cursor:pointer">
                    <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg"><g filter="url(#filter0_d)"><circle cx="36" cy="36" r="25" fill="white"/><circle cx="36" cy="36" r="25" stroke="#CBCBCB" stroke-opacity="0.5" stroke-width="0.5"/></g><rect x="32.5972" y="26" width="12.4028" height="5.27778" rx="2.63889" fill="#FF9463"/><rect x="32.5972" y="32.8611" width="12.4028" height="5.27778" rx="2.63889" fill="#FF9463"/><rect x="32.5972" y="39.7222" width="12.4028" height="5.27777" rx="2.63889" fill="#FF9463"/><rect x="26" y="26" width="5.40972" height="5.27778" rx="2.63889" fill="#FF9463"/><rect x="26" y="32.8611" width="5.40972" height="5.27778" rx="2.63889" fill="#FF9463"/><rect x="26" y="39.7222" width="5.40972" height="5.27777" rx="2.63889" fill="#FF9463"/><defs><filter id="filter0_d" x="0.75" y="0.75" width="70.5" height="70.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset/><feGaussianBlur stdDeviation="5"/><feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/><feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs></svg>
                    <p class="text-center" style="cursor:pointer"> Tugas </p>
                </div>
               <div class="text-center" style="cursor:pointer">
                   <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg"> <g filter="url(#filter0_d)"> <circle cx="36" cy="36" r="25" fill="white"/> <circle cx="36" cy="36" r="25" stroke="#CBCBCB" stroke-opacity="0.5" stroke-width="0.5"/> </g> <g clip-path="url(#clip0)"> <path d="M23 36.1667V34.8333C23 34.0259 23.4855 33.2977 24.2308 32.9872L39.4552 26.6436C40.3492 26.2711 41.391 26.5891 41.8621 27.4353C45.271 33.5576 44.6874 38.7599 41.8711 43.6121C41.3928 44.436 40.3673 44.7364 39.4879 44.37L24.2308 38.0128C23.4855 37.7023 23 36.9741 23 36.1667Z" fill="#FF9463"/> <rect x="27" y="37.5" width="7" height="7" rx="1" fill="#FF9463"/> <path d="M47.5028 34.342H45.6579C45.2946 34.342 45 34.6366 45 35C45 35.3634 45.2946 35.6579 45.6579 35.6579H47.5028C47.8661 35.6579 48.1607 35.3633 48.1607 35C48.1607 34.6366 47.8661 34.342 47.5028 34.342Z" fill="#FF9463"/> <path d="M47.4359 30.8291C47.2541 30.5144 46.8519 30.4066 46.5371 30.5883L45.3291 31.2857C45.0144 31.4673 44.9065 31.8697 45.0883 32.1844C45.2101 32.3956 45.4313 32.5135 45.6587 32.5135C45.7703 32.5135 45.8835 32.4851 45.987 32.4253L47.195 31.7278C47.5098 31.5462 47.6176 31.1438 47.4359 30.8291Z" fill="#FF9463"/> <path d="M47.195 38.2721L45.987 37.5746C45.6723 37.3929 45.27 37.5007 45.0883 37.8154C44.9065 38.1302 45.0144 38.5325 45.3291 38.7142L46.5371 39.4117C46.6407 39.4715 46.7539 39.4999 46.8654 39.4999C47.0929 39.4999 47.3139 39.3819 47.4359 39.1708C47.6176 38.8561 47.5098 38.4538 47.195 38.2721Z" fill="#FF9463"/> </g> <defs> <filter id="filter0_d" x="0.75" y="0.75" width="70.5" height="70.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"> <feFlood flood-opacity="0" result="BackgroundImageFix"/> <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/> <feOffset/> <feGaussianBlur stdDeviation="5"/> <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/> <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/> <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/> </filter> <clipPath id="clip0"> <rect width="25.161" height="19" fill="white" transform="translate(23 26)"/> </clipPath> </defs> </svg>
                    <p class="text-center" style="cursor:pointer"> Pengumuman </p>
                </div>
                <div class="text-center" style="cursor:pointer">
                    <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg"> <g filter="url(#filter0_d)"> <circle cx="36" cy="36" r="25" fill="white"/> <circle cx="36" cy="36" r="25" stroke="#CBCBCB" stroke-opacity="0.5" stroke-width="0.5"/> </g> <circle cx="36" cy="36" r="10" fill="#FF9463"/> <path d="M32 36.5L35 39.5L40 32" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> <defs> <filter id="filter0_d" x="0.75" y="0.75" width="70.5" height="70.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"> <feFlood flood-opacity="0" result="BackgroundImageFix"/> <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/> <feOffset/> <feGaussianBlur stdDeviation="5"/> <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/> <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/> <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/> </filter> </defs> </svg>
                    <p> Absensi </p>
                </div>
                <div class="text-center" style="cursor:pointer">
                    <svg width="72" height="72" viewBox="0 0 72 72" fill="none" xmlns="http://www.w3.org/2000/svg"> <g filter="url(#filter0_d)"> <circle cx="36" cy="36" r="25" fill="white"/> <circle cx="36" cy="36" r="25" stroke="#CBCBCB" stroke-opacity="0.5" stroke-width="0.5"/> </g> <rect x="27" y="27" width="8" height="8" rx="4" fill="#FF9463"/> <rect x="27" y="37" width="8" height="8" rx="4" fill="#FF9463"/> <rect x="37" y="27" width="8" height="8" rx="4" fill="#FF9463"/> <rect x="37" y="37" width="8" height="8" rx="4" fill="#FF9463"/> <defs> <filter id="filter0_d" x="0.75" y="0.75" width="70.5" height="70.5" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"> <feFlood flood-opacity="0" result="BackgroundImageFix"/> <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/> <feOffset/> <feGaussianBlur stdDeviation="5"/> <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/> <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow"/> <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape"/> </filter> </defs> </svg>
                    <p> Lainnya </p>
                </div>
            </div>
            <div class="title mt-4" id="title-available-contract">Jangan sampai kelewat....!!</div>
            <div class="pengumuman-card" style="background-image: url('./../icon/banner/banner-home.svg');">
            </div>
            <div class="help" onclick="gotoUrl('https:/\/wa.me/6285640372959', 'blank')">
                <div class="title">Bantuan</div>
                <svg width="46" height="46" viewBox="0 0 46 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M23 7.1875C19.8726 7.1875 16.8154 8.11489 14.2151 9.85239C11.6147 11.5899 9.58797 14.0595 8.39116 16.9488C7.19435 19.8382 6.88121 23.0175 7.49134 26.0849C8.10147 29.1522 9.60746 31.9697 11.8189 34.1811C14.0303 36.3925 16.8478 37.8985 19.9151 38.5087C22.9825 39.1188 26.1618 38.8057 29.0512 37.6088C31.9405 36.412 34.4101 34.3853 36.1476 31.785C37.8851 29.1846 38.8125 26.1274 38.8125 23C38.8125 18.8063 37.1466 14.7843 34.1811 11.8189C31.2157 8.85346 27.1937 7.1875 23 7.1875Z" fill="#30AEE4" stroke="#30AEE4" stroke-width="2.5" stroke-miterlimit="10"/>
                    <path d="M23.2003 25.92C22.9736 25.92 22.787 25.84 22.6403 25.68C22.507 25.52 22.4403 25.3 22.4403 25.02C22.4403 24.2867 22.5736 23.6667 22.8403 23.16C23.107 22.6533 23.4936 22.1 24.0003 21.5C24.4003 21.02 24.6936 20.62 24.8803 20.3C25.067 19.98 25.1603 19.62 25.1603 19.22C25.1603 18.78 24.9936 18.4333 24.6603 18.18C24.3403 17.9133 23.9003 17.78 23.3403 17.78C22.8603 17.78 22.4003 17.8867 21.9603 18.1C21.5203 18.3 21.007 18.5933 20.4203 18.98C20.087 19.1667 19.8136 19.26 19.6003 19.26C19.3736 19.26 19.1736 19.16 19.0003 18.96C18.8403 18.7467 18.7603 18.5 18.7603 18.22C18.7603 17.8333 18.9203 17.52 19.2403 17.28C19.8003 16.8 20.4603 16.4267 21.2203 16.16C21.9803 15.88 22.747 15.74 23.5203 15.74C24.3336 15.74 25.0536 15.88 25.6803 16.16C26.3203 16.44 26.8136 16.8267 27.1603 17.32C27.5203 17.8133 27.7003 18.38 27.7003 19.02C27.7003 19.7133 27.5336 20.32 27.2003 20.84C26.867 21.3467 26.3803 21.92 25.7403 22.56C25.1936 23.12 24.7803 23.5933 24.5003 23.98C24.2336 24.3533 24.067 24.78 24.0003 25.26C23.9603 25.4733 23.867 25.64 23.7203 25.76C23.587 25.8667 23.4136 25.92 23.2003 25.92ZM23.2403 30.12C22.8136 30.12 22.4603 29.98 22.1803 29.7C21.9003 29.4067 21.7603 29.0467 21.7603 28.62C21.7603 28.1933 21.9003 27.84 22.1803 27.56C22.4603 27.2667 22.8136 27.12 23.2403 27.12C23.667 27.12 24.0203 27.2667 24.3003 27.56C24.5803 27.84 24.7203 28.1933 24.7203 28.62C24.7203 29.0467 24.5736 29.4067 24.2803 29.7C24.0003 29.98 23.6536 30.12 23.2403 30.12Z" fill="white"/>
                </svg>
            </div>
        </div>
    </div>

    <!-- Welcome Message Modal -->
    <div class="modal fade" id="welcome-message-modal" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-sm modal-dialog modal-dialog-centered">
            <div class="modal-content welcome-modal">
                <div class="modal-body text-center">
                    <img src="{{ asset('assets/images/icon_check_auth.svg') }}" alt="">
                    <h4 class="title">Hai, <span class="show-data-user-name">{{ Session::get('user')->name ?? '-' }}</span></h4>
                    <p class="description">Selamat datang di dasbor layanan Personal Waste Management Anda</p>
                    <div class="footer">
                        <button class="btn btn-rounded btn-success" id="btn-close-welcome-message-modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js-library')
    <script src="{{ asset('assets/lib/swiper-5.4.5/js/swiper.min.js') }}"></script>
@endsection

@section('js-inline')
    <script>
        var orderFailedNotification = "";

        $(function () {
            $('#bottom-navigation-home').addClass('active', 'active');
            if(!Cookies.get('welcome_modal_home')) {
                $('#welcome-message-modal').modal('show');
            }

            $('#btn-close-welcome-message-modal').on('click', function () {
                $('#welcome-message-modal').modal('hide');

                Cookies.set('welcome_modal_home', true, { expires: 30 });
            });

            if(orderFailedNotification != '') {
                notification(orderFailedNotification);
            }

            getUser();
            getContract();
            getNotification();
        });

        function gotoUrl(url, type = 'in') {
            if(type == 'in') {
                window.location= url;
            } else if(type == 'blank') {
                window.open(url, '_blank');
            }
        }

        function callbackGetUser(data) {
            var user = data.data;

            if(data.success) {
                $('.show-data-user-name').text((user.name == null ? '-' : user.name));
            } else {
                if(data.message instanceof Array) {
                    if(data.message[0] == 'Unauthenticated.') {
                        window.location="@route('logout')";
                    } else {
                        notification(data.message[0]);
                    }
                } else {
                    notification(data.message);
                }
            }
        }

        function beforeSendContract() {
            $('#page-nothing-contract').hide();
            $('#title-available-contract').show();
            $('#page-available-contract').show();
            $('#page-available-contract .box-statistic .loading-minor-page').show();
            $('#page-available-contract .box-statistic .title').hide();
            $('#page-available-contract .box-statistic .content').hide();
        }

        function completeSendContract() {

        }

        function callbackGetContract(data) {
            var contracts = data.data;

            if(data.success) {
                var countNotActive = 0;

                contracts.forEach(contract => {
                    if(contract.contract.status != 'ACTIVE') {
                        countNotActive += 1;
                    }
                });

                if(countNotActive != 0) {
                    var urlAlert = "@route('contract')";

                    if(countNotActive == 1) {
                        urlAlert = "@route('contract')/" + contracts[0].id;
                    }

                    var htmlShowAlert = '';
                    htmlShowAlert += '<div class="title">'+countNotActive+' Pemberitahuan</div>';
                    htmlShowAlert += '<a href="'+urlAlert+'">';
                    htmlShowAlert += '<svg width="13" height="12" viewBox="0 0 13 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M6.74781 11.6886C6.67324 11.6142 6.61407 11.5258 6.5737 11.4285C6.53333 11.3312 6.51255 11.2269 6.51255 11.1216C6.51255 11.0163 6.53333 10.912 6.5737 10.8147C6.61407 10.7174 6.67324 10.629 6.74781 10.5546L10.9873 6.31674L6.74781 2.07886C6.67336 2.0044 6.6143 1.91601 6.574 1.81873C6.53371 1.72145 6.51297 1.61719 6.51297 1.51189C6.51297 1.40659 6.53371 1.30233 6.574 1.20505C6.6143 1.10776 6.67336 1.01937 6.74781 0.944916C6.82227 0.87046 6.91066 0.811398 7.00794 0.771103C7.10522 0.730808 7.20949 0.710069 7.31479 0.710069C7.42008 0.710069 7.52435 0.730808 7.62163 0.771103C7.71891 0.811398 7.8073 0.87046 7.88176 0.944916L12.6866 5.74977C12.7612 5.82415 12.8204 5.91253 12.8607 6.00982C12.9011 6.10711 12.9219 6.2114 12.9219 6.31674C12.9219 6.42207 12.9011 6.52637 12.8607 6.62366C12.8204 6.72095 12.7612 6.80932 12.6866 6.88371L7.88176 11.6886C7.80737 11.7631 7.719 11.8223 7.62171 11.8627C7.52442 11.903 7.42012 11.9238 7.31479 11.9238C7.20945 11.9238 7.10515 11.903 7.00786 11.8627C6.91057 11.8223 6.8222 11.7631 6.74781 11.6886Z" fill="#BC8210"/><path fill-rule="evenodd" clip-rule="evenodd" d="M12.1191 6.31638C12.1191 6.52877 12.0348 6.73246 11.8846 6.88264C11.7344 7.03282 11.5307 7.11719 11.3183 7.11719L0.907825 7.11719C0.695436 7.11719 0.491747 7.03282 0.341567 6.88263C0.191386 6.73245 0.107016 6.52877 0.107016 6.31638C0.107016 6.10399 0.191386 5.9003 0.341567 5.75012C0.491747 5.59994 0.695437 5.51557 0.907825 5.51557L11.3183 5.51557C11.5307 5.51557 11.7344 5.59994 11.8846 5.75012C12.0348 5.9003 12.1191 6.10399 12.1191 6.31638Z" fill="#BC8210"/></svg>';
                    htmlShowAlert += '</a>';

                    $('.notification-alert').html(htmlShowAlert);
                    $('.notification-alert').show();
                }

                if(contracts.length == 0) {
                    $('#page-nothing-contract').show();
                    $('#title-available-contract').hide();
                    $('#page-available-contract').hide();
                    $('#request-new-contract').hide();
                } else {
                    $('#page-nothing-contract').hide();
                    $('#title-available-contract').show();
                    $('#page-available-contract').show();
                    $('#request-new-contract').show();
                    $('#page-available-contract .box-statistic .loading-minor-page').hide();
                    $('#page-available-contract .box-statistic .title').show();
                    $('#page-available-contract .box-statistic .content').show();
                    $('#page-available-contract .box-statistic.service .content .count').text(contracts.length);
                }
            } else {
                if(data.message instanceof Array) {
                    if(data.message[0] == 'Unauthenticated.') {
                        window.location="@route('logout')";
                    } else {
                        notification(data.message[0]);
                    }
                } else {
                    notification(data.message);
                }
            }
        }

        function beforeSendNotification()
        {

        }

        function completeSendNotification()
        {

        }

        function callbackGetNotification(data)
        {
            if(data.success) {
                if(data.data.total == 0) {
                    $('.notification .icon-notification').attr("src", "@asset('assets/images/icon_notification_black.svg')");
                } else {
                    var countUnread = 0;

                    data.data.data.forEach((notification, i) => {
                        if(notification.is_read == 0) {
                            countUnread++;
                        }
                    });

                    if(countUnread > 0) {
                        $('.notification .icon-notification').attr("src", "@asset('assets/images/icon_notification_new_black.svg')");
                    } else {
                        $('.notification .icon-notification').attr("src", "@asset('assets/images/icon_notification_black.svg')");
                    }
                }
            } else {
                if(data.message instanceof Array) {
                    if(data.message[0] == 'Unauthenticated.') {
                        window.location="@route('logout')";
                    } else {
                        notification(data.message[0]);
                    }
                } else {
                    notification(data.message);
                }
            }
        }

        var swiper = new Swiper('.swiper-container', {
            spaceBetween: 18,
            slidesPerView: 'auto',
            centeredSlides: false,
            loop: false
        });
    </script>
@endsection
