<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    public function index(){
        return view('auth.register.index');
    }

    public function register(Request $request){
        if($request->query('verifikasi') == 'whatsapp'){
            return view('auth.register.verification.whatsapp');
        }
    }
}
