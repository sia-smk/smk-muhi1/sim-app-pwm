<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index(){
        return view('auth.login.index');
    }

    public function  verificationWhatsapp() {
        $data['request_again'] = 60;
        return view('auth.login.verification.whatsapp')->with($data);
    }
}
