<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('iframe-test');
});

Route::get('home', 'HomeController@home');
Route::name('login')->prefix('/login')->group(function () {
    Route::get('/', 'Auth\LoginController@index');
    Route::post('/', 'Auth\LoginController@login');

    Route::name('.verification')->prefix('/verification')->group(function () {
        Route::get('/method', 'Auth\LoginController@verificationMethod')->name('.method');

        Route::get('/whatsapp', 'Auth\LoginController@verificationWhatsapp')->name('.whatsapp');
        Route::get('/sms', 'Auth\LoginController@verificationSms')->name('.sms');
        // Route::get('/email', 'Auth\LoginController@verificationEmail')->name('.email');

        Route::post('/request', 'Auth\LoginController@request')->name('.request');
        Route::post('/verification', 'Auth\LoginController@verification');
    });
});


Route::group(['prefix' => 'register'], function () {
    Route::get('/', 'Auth\RegisterController@index');
    Route::post('/', 'Auth\RegisterController@register');
    
});


/**
 * Notification Routes
*/
Route::name('notification')->prefix('/notifications')->group(function () {
    Route::get('/', 'NotificationController@index');
    Route::post('/', 'NotificationController@getNotification');
    Route::post('/save-user-device-token', 'NotificationController@saveUserDeviceToken')->name('.saveUserDeviceToken');
    Route::get('/{notificationId}', 'NotificationController@readNotification')->name('.detail');
});
